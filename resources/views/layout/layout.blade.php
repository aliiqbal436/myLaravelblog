<html>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Blog</title>
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mdb.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
        <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>

    </head>
    <body>
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark primary-color">
               <div class="container">
                   <a class="navbar-brand" href="#">MyBlog</a>
                   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                       <span class="navbar-toggler-icon"></span>
                   </button>

                   <div class="collapse navbar-collapse" id="navbarSupportedContent">
                       <ul class="navbar-nav mr-auto">
                           <li class="nav-item active">
                               <a class="nav-link" href="{{ route('index') }}"> Home <span class="sr-only">(current)</span></a>
                           </li>
                           <li class="nav-item">
                               <a class="nav-link" href="{{ route('user.create') }}"> Create User </a>
                           </li>
                           <li class="nav-item">
                               <a class="nav-link" href="{{ route('post.create') }}"> Create Post </a>
                           </li>
                           <li class="nav-item">
                               <a class="nav-link" href="{{ route('user.index') }}"> Users </a>
                           </li>
                       </ul>
                   </div>
               </div>
            </nav>
        </header>
        <div class="content">
            @yield('content')
        </div>

        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/select2.full.min.js') }}"></script>
        <script src="{{ asset('js/script.js') }}"></script>
    </body>
</html>