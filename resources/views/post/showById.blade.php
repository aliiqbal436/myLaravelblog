@extends('layout.layout')
@section('content')
    <div class="container">
        <div class="wrapper">
            <div class="col-md-8 pull-left post-detail-div">

                <div class="post-title">
                    <h2>{{ $post->title }}</h2>
                    <small><em>{{ $post->name }} : {{ $post->created_at }}</em></small>
                </div>
                <div class="post-detail-body">
                    <p class="post-text">
                        {{ $post->text }}
                    </p>
                </div>
                <div class="like-panel pull-left">
                    <a class="btn btn-outline-red btn-sm" id="like-btn" data-token="{{ csrf_token() }}" data-id="{{ $post->id }}"><i class="far fa-thumbs-up"></i> <span class="like-count">{{ $likes  }}</span></a>
                </div>
                <div class="like-panel pull-right">
                    <a class="btn btn-outline-black btn-sm" href=" {{ route('post.select', $post->id) }}"><i class="fas fa-edit"></i></a>
                    <a class="btn btn-outline-green btn-sm" href="{{ route('post.delete', $post->id) }}"><i class="fas fa-trash-alt"></i>
                    </a>
                </div>
                <div class="comment-panel ">
                    <form action=" {{ route('comment.save') }}" method="post" id="comment-form">
                        <input type="hidden" id="user-id-for-like-co" name="user_id" value="">
                        <input type="hidden" name="post_id" value="{{ $post->id }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <input type="text" class="form-control" id="comment-input" name="text" placeholder="Enter your comments">
                        </div>
                    </form>
                </div>
                <div class="comment-main-div">
                    @foreach($comments as $comment)

                        <div class="comment-div">
                            <h5>{{ $comment->name }}</h5>
                            <p class="margin-bottom-0">
                                {{ $comment->text }}
                            </p>
                            <div class="like-panel pull-right">
                                <a class="btn btn-outline-black btn-sm margin-top-bottom-0 edit-comment-btn" data-id="{{ $comment->id }}" data-text="{{ $comment->text }}"><i class="fas fa-edit"></i></a>
                                <a class="btn btn-outline-green btn-sm margin-top-bottom-0" href="{{ route('comment.delete', $comment->id) }}"><i class="fas fa-trash-alt"></i>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-4 pull-left">
                <h5>Select user for like and comment</h5>
                <select name="user_id" id="user-select-for-like-co" class="select-user form-control">
                    <option value="" default disabled selected>Select User</option>
                    @foreach($users as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
@endsection