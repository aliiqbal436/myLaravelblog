@extends('layout.layout')
@section('content')
    <div class="col-md-4 offset-md-4 create-user-div">
        <h1 class="create-heading">Update Post</h1>
        <form action="{{ route('post.edit', $post->id) }}" method="post">
            <div class="form-group">
                @if($errors->has('name'))
                    <p class="error">{{$errors->first('user_id')}}</p>
                @endif
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Post title</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="Enter Your Post Title" value="{{ $post->title }}">
                @if($errors->has('name'))
                    <p class="error">{{$errors->first('father_name')}}</p>
                @endif
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Post Text</label>
                <textarea type="text" class="form-control" id="post-text" name="text" placeholder="Enter Your Post Text" >{{ $post->text }}</textarea>
                @if($errors->has('name'))
                    <p class="error">{{$errors->first('email')}}</p>
                @endif
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-outline-blue btn-md pull-right">Update</button>
        </form>
    </div>
@endsection