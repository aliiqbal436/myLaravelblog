@extends('layout.layout')
@section('content')
    <div class="col-md-4 offset-md-4 create-user-div">
        <h1 class="create-heading">Create Post</h1>
        <form action="{{ route('post.save') }}" method="post">
            <div class="form-group">
                <label for="exampleInputEmail1">Select User</label>
                <select name="user_id" class="select-user form-control">
                    @foreach($users as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                </select>
                @if($errors->has('name'))
                    <p class="error">{{$errors->first('user_id')}}</p>
                @endif
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Post title</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="Enter Your Post Title">
                @if($errors->has('name'))
                    <p class="error">{{$errors->first('father_name')}}</p>
                @endif
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Post Text</label>
                <textarea type="text" class="form-control" id="post-text" name="text" placeholder="Enter Your Post Text"></textarea>
                @if($errors->has('name'))
                    <p class="error">{{$errors->first('email')}}</p>
                @endif
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-outline-blue pull-right">Submit</button>
        </form>
    </div>
@endsection