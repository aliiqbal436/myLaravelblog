@extends('layout.layout')
@section('content')
    <div class="container">
        <div class="col-md-6 post-heading">
            <h2>All Posts</h2>
        </div>
        @foreach($posts as $post)
            <div class="col-md-8 post-div">
                <div class="post-title">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3 class="pull-left">{{ $post->title }}</h3>
                        </div>
                        <div class="col-sm-5">
                            <h6 class="pull-left">
                                <small><em>{{ $post->name }} : {{ $post->created_at }}</em></small>
                            </h6>
                        </div>
                    </div>
                </div>

                <div class="post-body">
                    <p>
                        {{ $post->text }}
                    </p>
                    <a href="{{ route('post.detail',$post->id) }}">Read more</a>
                </div>
            </div>
        @endforeach
    </div>
@endsection