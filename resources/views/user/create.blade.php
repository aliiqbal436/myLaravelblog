@extends('layout.layout')
@section('content')
    <div class="col-md-4 offset-md-4 create-user-div">
        <h1 class="create-heading">Create User</h1>
        <form action="{{ route('user.save') }}" method="post">
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Your Name">
                @if($errors->has('name'))
                    <p class="error">{{$errors->first('name')}}</p>
                @endif
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Father Name</label>
                <input type="text" class="form-control" id="father-name" name="father_name" placeholder="Enter Your Father Name">
                @if($errors->has('name'))
                    <p class="error">{{$errors->first('father_name')}}</p>
                @endif
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="text" class="form-control" id="father-name" name="email" placeholder="Enter Your Email">
                @if($errors->has('name'))
                    <p class="error">{{$errors->first('email')}}</p>
                @endif
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-outline-blue pull-right">Submit</button>
        </form>
    </div>
@endsection