@extends('layout.layout')
@section('content')
    <div class="container">
        <div class="wrapper">
            <table class="table table-dark table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Father Name</th>
                        <th>Email</th>
                        <th>Process</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->father_name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                <a class="btn btn-outline-info btn-sm" href=" {{ route('user.edit', ['id' => $user->id]) }}"><i class="fas fa-edit"></i></a>
                                <a class="btn btn-outline-red btn-sm" href="{{ route('user.delete', $user->id) }}"><i class="fas fa-trash-alt"></i>
                            </td>
                        </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
    </div>
@endsection