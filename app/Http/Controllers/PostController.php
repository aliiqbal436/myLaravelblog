<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User as user;
use App\Models\Post as Post;
use App\Http\Controllers\CommentController as commentCo;
use App\Http\Controllers\LikeController as LikeCo;
class PostController extends Controller
{
    public function index(){
        $posts = Post::join('users', 'users.id', '=', 'posts.user_id')
            ->select('users.*', 'posts.*')
            ->get();

        return view('home/index', ['posts' => $posts]);
    }

    public function showById($id, commentCo $comment, LikeCo $like){
        $post = Post::join('users', 'users.id', '=', 'posts.user_id')
            ->select('users.*', 'posts.*')
            ->where('posts.id',$id)
            ->first();
        $users = user::all();
        $likes = $like->countLikes($id);
        $comments = $comment->showByPost($id);
        return view('post/showById',['post' => $post,'users' => $users, 'comments' => $comments, 'likes' => $likes]);
    }

    public function select($id){
        $post = Post::where('id',$id)->first();
        return view('post/edit',['post' => $post]);
    }

    public function create(){
        $users = user::all();
        return view('post/create',['users' => $users]);
    }

    public function save(Request $request){
        $request = $request->validate([
            'user_id' => 'required',
            'title' => 'required',
            'text' => 'required',
        ]);
        Post::create($request);
        return redirect()->route('index');
    }


    public function edit(Request $request,$id){
        $request = $request->validate([
            'title' => 'required',
            'text' => 'required',
        ]);
        Post::where('id',$id)->update($request);
        return redirect()->back();
    }

    public function delete($id){
        $post = Post::find($id);
        $post->delete();
        return redirect()->route('index');
    }
}
