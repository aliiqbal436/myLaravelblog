<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User as user;
class UserController extends Controller
{
    public function index(){
        $users = user::all();
        return view('user.index',['users' => $users]);
    }

    public function create(){
        return view('user/create');
    }

    public function save(Request $request){
        $request = $request->validate([
            'name' => 'required',
            'father_name' => 'required',
            'email' => 'required',
        ]);
        user::create($request);
        return redirect()->route('index');
    }

    public function edit($id){
        $user = user::where('id',$id)->first();
        return view('user/edit', ['user' => $user]);
    }

    public function update(Request $request, $id){
        $request = $request->validate([
            'name' => 'required',
            'father_name' => 'required',
            'email' => 'required',
        ]);
        user::where('id',$id)->update($request);
        return redirect()->route('user.index');
    }

    public function delete($id){
        $user = user::find($id);
        $user->delete();
        return redirect()->back();
    }
}
