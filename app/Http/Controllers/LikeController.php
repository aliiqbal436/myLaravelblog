<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Like as like;
class LikeController extends Controller
{
    public function countLikes($id){
        $like_count = like::where('post_id',$id)->count();
       // dd($like_count);
        return $like_count;
    }

    public function select($id){
        $user_count = like::where('user_id',$id)->count();
        return $user_count;
    }

    public function add(Request $request){
        //dd($request->post_id);
        $user_id = $request->user_id;
        $user_count = $this->select($user_id);

        $request = $request->validate([
            'post_id' => 'required',
            'user_id' => 'required',
        ]);
        if ($user_count == 1){
            like::where('user_id',$user_id)->delete();
        }
        if ($user_count == 0){
            like::create($request);
        }


    }

}
