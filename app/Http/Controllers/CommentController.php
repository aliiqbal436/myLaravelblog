<?php

namespace App\Http\Controllers;

use App\Models\Comment as Comment;
use App\Post;
use function Couchbase\defaultDecoder;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function save(Request $request){

        $request = $request->validate([
            'user_id' => 'required',
            'post_id' => 'required',
            'text' => 'required',
        ]);

        Comment::create($request);
        return redirect()->back();
    }

    public function showByPost($id){
         $comments = Comment::join('users', 'users.id', '=', 'comments.user_id')
             ->select('comments.*','users.name')
             ->where('comments.post_id',$id)
             ->get();
         //dd($comments);
         return $comments;
    }

    public function edit(Request $request, $id){
        $request = $request->validate([
            'text' => 'required',
        ]);
        $comment = Comment::where('id',$id)->update($request);
        return redirect()->back();
    }

    public function delete($id){
        $comment = Comment::find($id);
        //dd($comment);
        $comment->delete();
        return redirect()->back();
    }
}
