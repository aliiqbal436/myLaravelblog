<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', 'HomeController@index')->name('index');
Route::get('/', 'PostController@index')->name('index');
Route::get('/user/create', 'UserController@create')->name('user.create');
Route::post('/user/save', 'UserController@save')->name('user.save');
Route::get('/post/create', 'PostController@create')->name('post.create');
Route::post('/post/save', 'PostController@save')->name('post.save');
Route::get('/post/{id}/detail', 'PostController@showById')->name('post.detail');
Route::post('/comment/save', 'CommentController@save')->name('comment.save');
Route::get('/post/{id}/select', 'PostController@select')->name('post.select');
Route::post('/post/{id}/edit', 'PostController@edit')->name('post.edit');
Route::get('/post/{id}/delete', 'PostController@delete')->name('post.delete');
Route::post('/comment/{id}/edit', 'CommentController@edit')->name('comment.edit');
Route::get('/comment/{id}/delete', 'CommentController@delete')->name('comment.delete');
Route::get('/users', 'UserController@index')->name('user.index');
Route::get('/user/{id}/edit', 'UserController@edit')->name('user.edit');
Route::post('/user/{id}/update', 'UserController@update')->name('user.update');
Route::get('/user/{id}/delete', 'UserController@delete')->name('user.delete');
Route::post('/like/add', 'LikeController@add')->name('like.add');