$(window).on("load",function(){
    var userSelectForLikeCo;
    $(".select-user").select2();
    $("#user-select-for-like-co").change(function () {
        userSelectForLikeCo = $("#user-select-for-like-co option:selected").val();
       $("#user-id-for-like-co").val(userSelectForLikeCo);
    });
    $(".edit-comment-btn").click(function () {
        var commentId = $(this).data('id');
        var commentText = $(this).data('text');
        $("#comment-input").val(commentText);
        $("#comment-form").attr('action', "http://localhost:8000/comment/"+commentId+"/edit");
    });

    $("#like-btn").click(function () {
        var postId = $(this).data('id');
        var token = $(this).data('token');
        if (userSelectForLikeCo > 0){
            $.ajax({

                method: 'POST',
                url: "/like/add",
                data: { post_id:postId, user_id: userSelectForLikeCo, _token:token},
                success:function (result) {

                }
            });
        }else{
            alert('Select user first');
        }
    });
});